package model;

import java.util.Date;

public class Operation {

	public enum Type {
		DEPOSIT, WITHDRAWAL
	}

	private Type type;
	private Date date;
	private double ammount;
	private double balance;

	public Operation(Type type, Date date, double ammount, double balance) {
		super();
		this.type = type;
		this.date = date;
		this.ammount = ammount;
		this.balance = balance;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getAmmount() {
		return ammount;
	}

	public void setAmmount(double ammount) {
		this.ammount = ammount;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

}
