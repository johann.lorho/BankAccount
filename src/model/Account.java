package model;

public class Account {

	private String id;
	private double balance;
	private double overdraftAllowed;

	public Account(String id, double balance, double overdraftAllowed) {
		super();
		this.id = id;
		this.balance = balance;
		this.overdraftAllowed = overdraftAllowed;
	}

	public String getId() {
		return id;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getOverdraftAllowed() {
		return overdraftAllowed;
	}

}
