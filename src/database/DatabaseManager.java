package database;

import java.security.InvalidParameterException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Account;

public class DatabaseManager {

	static final String JDBC_DRIVER = "org.h2.Driver";
	static final String DB_URL = "jdbc:h2:~/test";
	static final String DB_USER = "admin";
	static final String DB_PASS = "admin";

	public static void depositQuery(Account account) {
		Connection conn = null;
		Statement stmt = null;
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
			stmt = conn.createStatement();
//			String query1 = "CREATE TABLE ACCOUNT (id STRING not NULL, amount FLOAT,  PRIMARY KEY ( id ))";
			String query = String.format("select id from ACCOUNT where id = %s", account.getId());
			ResultSet rs = stmt.executeQuery(query);
			if(!rs.next()) {
				throw new InvalidParameterException(String.format("Account id %s does not exist", account.getId()));
			}
			
			query = String.format("update amount from ACCOUNT where id = %s", account.getId());
			
			

			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
	}
	


}
