package api;

import model.Account;

public class OperationRequest {

	private Account account;
	private double amount;

	public Account getAccount() {
		return account;
	}

	public double getAmount() {
		return amount;
	}
}
