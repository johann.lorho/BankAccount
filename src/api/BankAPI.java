package api;

import java.rmi.server.Operation;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import model.Account;

@Path("/bank")
public class BankAPI {

	@POST
	@Path("/deposit")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deposit(OperationRequest request) {
		if (request == null || request.getAccount() == null || request.getAmount() <= 0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		Account account = request.getAccount();
		account.setBalance(account.getBalance() + request.getAmount());

		// TODO
		// Check the account exists
		// Save the deposit operation in the database
		// checkAccount(account);
		// saveOperation(request.getAmount(), "DEPOSIT", account);

		return Response.ok(account).build();
	}

	@POST
	@Path("/withdraw")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response withdraw(OperationRequest request) {
		// Validate the request and make sure the amount is positive and not greater
		// than the available balance
		if (request == null || request.getAccount() == null || request.getAmount() <= 0
				|| request.getAmount() > request.getAccount().getBalance()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		Account account = request.getAccount();
		account.setBalance(account.getBalance() - request.getAmount());

		// TODO
		// Check the account exists and withdrawal is possible
		// Save the withdrawal operation in the database
		// checkAccount(account);
		// saveOperation(request.getAmount(), "WITHDRAWAL", account);

		// Return the updated account object in the response
		return Response.ok(account).build();
	}

	@POST
	@Path("/history")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response history(OperationRequest request) {
		// Validate the request
		if (request == null || request.getAccount() == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		Account account = request.getAccount();

		// TODO
		// Retrieve historicized operations for the account, if it exists
		// getHistory(account);

		// Return the updated account object in the response
		return Response.ok(account).build();
	}

}